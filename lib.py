import vkLib
import os
import qrencoder as qr
import imgprocessor as ip
import cryptor as cv

def onMessageIncoming(user, message, attachments):
    if user.updStep() == vkLib.Stages.MAIN or message == 'Вернуться в начало':
        user.updStep(vkLib.Stages.TYPE)
        user.write('Здравствуйте, ' + user.getName() + '.\nВас приветствует General.\n\nВыберите операцию, которую хотите выполнить, при помощи нажатия на соответствующую кнопку или вводом цифры из списка:\n' + vkLib.Answers.TYPE.getList('&#128313; '), keyboard=vkLib.Answers.TYPE.getKeyboard())

    elif user.updStep() == vkLib.Stages.TYPE:
        if message.isnumeric(): actionid = vkLib.Answers.TYPE.getActionId(id=int(message))
        else: actionid = vkLib.Answers.TYPE.getActionId(text=message)
        if actionid != None:
            if actionid in range(1,3):
                user.updStep(vkLib.Stages.CIPH_TYPE)
                if actionid == 1: user.cryptor = cv.Caesar()
                else: user.cryptor = cv.Viginer()
                user.write('Вы выбрали "' + vkLib.Answers.TYPE.buttons[actionid-1].text + '".\nКакое действие потребуется выполнить?\n\nВыберите режим при помощи кнопки или вводом цифры из списка:\n' + vkLib.Answers.CIPH.getList('&#128313; '), keyboard=vkLib.Answers.CIPH.getKeyboard())
            elif actionid == 3:
                user.updStep(vkLib.Stages.QRCODE)
                user.write('Вы выбрали операцию по преобразованию текста в QR-код.\nРядовой,введите текст, который хотите преобразовать: ', keyboard=vkLib.Answers.BACK.getKeyboard())
            elif actionid == 4:
                user.updStep(vkLib.Stages.IMGBLUR)
                user.write('Вы выбрали операцию размытию фотографий.\nЗагрузите фотографии и отправьте их для наложения эффекта размытия.', keyboard=vkLib.Answers.BACK.getKeyboard())
        else:
            user.write('&#10071; Произошла ошибка! Такого действия не существует.\nПовторите попытку нажатием на соответствующую кнопку или введите нужную цифру из списка:\n' + vkLib.Answers.TYPE.getList('&#128312; '), keyboard=vkLib.Answers.TYPE.getKeyboard())

    elif user.updStep() == vkLib.Stages.CIPH_TYPE:
        if message.isnumeric(): actionid = vkLib.Answers.CIPH.getActionId(id=int(message))
        else: actionid = vkLib.Answers.CIPH.getActionId(text=message)
        if actionid != None:
            if actionid == 1: user.cryptor.setMode(cv.CipherModes.ENCRYPT)
            else: user.cryptor.setMode(cv.CipherModes.DECRYPT)
            if user.cryptor.getKey() != None:
                user.updStep(vkLib.Stages.CIPH_MSG)
                user.write('Вы выбрали режим "' + vkLib.Answers.CIPH.buttons[actionid-1].text + '".\n&#128196; Введите текст, который необходимо преобразовать:', keyboard=vkLib.Answers.BACK.getKeyboard())
            else:
                user.updStep(vkLib.Stages.CIPH_KEY)
                user.write('Вы выбрали режим "' + vkLib.Answers.CIPH.buttons[actionid-1].text + '".\n&#128272; Введите ключ, который будет использоваться для преобразования текста:', keyboard=vkLib.Answers.BACK.getKeyboard())
        else:
            user.write('&#10071; Произошла ошибка! Такого действия не существует.\nПовторите попытку нажатием на соответствующую кнопку или введите нужную цифру из списка:\n' + vkLib.Answers.CIPH.getList('&#128312; '), keyboard=vkLib.Answers.CIPH.getKeyboard())

    elif user.updStep() == vkLib.Stages.CIPH_KEY:
        if len(message) > 0:
            user.updStep(vkLib.Stages.CIPH_MSG)
            user.cryptor.setKey(message)
            user.write('Вы успешно установили ключ.\n&#128196; Теперь введите, который необходимо преобразовать:')
        else:
            user.write('&#10071; Произошла ошибка! Ваше сообщение не содержит текста.\nПовторите попытку ввода ключа:', keyboard=vkLib.Answers.BACK.getKeyboard())

    elif user.updStep() == vkLib.Stages.CIPH_MSG:
        if len(message) > 0:
            user.updStep(vkLib.Stages.MAIN)
            user.write('&#10004; Текст успешно преобразован. Результат:\n' + user.cryptor.cipher(message), keyboard=vkLib.Answers.BACK.getKeyboard())
        else:
            user.write('&#10071; Произошла ошибка! Ваше сообщение не содержит текста.\nПовторите попытку ввода текста:', keyboard=vkLib.Answers.BACK.getKeyboard())

    elif user.updStep() == vkLib.Stages.QRCODE:
        if len(message) > 0:
            user.updStep(vkLib.Stages.MAIN)
            qrcode = qr.QREncoder.encode(message)
            user.write('&#10004; Текст успешно преобразован в QR-код.', attachment=qrcode, keyboard=vkLib.Answers.BACK.getKeyboard())
            os.remove(qrcode)
        else:
            user.write('&#10071; Произошла ошибка! Ваше сообщение не содержит текста.\nПовторите попытку ввода текста:', keyboard=vkLib.Answers.BACK.getKeyboard())

    elif user.updStep() == vkLib.Stages.IMGBLUR:
        photos = attachments.getPhotos()
        if photos is not None:
            user.updStep(vkLib.Stages.MAIN)
            for p in photos:
                ip.ImageProcessor.addBlur(p)
            user.write('&#10004; Фотографии успешно преобразованы.', attachment=photos, keyboard=vkLib.Answers.BACK.getKeyboard())
            for p in photos:
                os.remove(p)
        else:
            user.write('&#10071; Произошла ошибка! Прикрепите фотографии к вашему сообщению.', keyboard=vkLib.Answers.BACK.getKeyboard())

def main():
    bot = vkLib.botVk();
    bot.listen(onMessageIncoming);

if __name__ == '__main__':
    main()
