from PIL import Image, ImageFilter

class ImageProcessor():
    def addBlur(imgPath, savePath=None):
        if savePath is None: savePath = imgPath
        img = Image.open(imgPath)
        img = img.filter(ImageFilter.BoxBlur(15))
        img.save(savePath)
        img.close()
        return savePath
